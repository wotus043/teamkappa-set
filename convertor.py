from json import JSONEncoder

calls = []
responses =[]

with open("Calls.txt", encoding="utf-8") as f:
    for line in f:
        calls.append(line.rstrip("\n"))

with open("Responses.txt", encoding="utf-8") as f:
    for line in f:
        responses.append(line.rstrip("\n"))

with open("Responses_json.txt", encoding="utf-8", mode="w") as f:
    f.write(JSONEncoder(ensure_ascii=False).encode(responses))

with open("Calls_json.txt", encoding="utf-8", mode="w") as f:
    f.write(JSONEncoder(ensure_ascii=False).encode(calls))
